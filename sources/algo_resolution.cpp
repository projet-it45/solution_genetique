#include "../headers/algo_resolution.h"

using namespace std;

Algo_Resolution::Algo_Resolution(int taille_population, float taux_croisement, float taux_mutation, float temps_limite_secondes)
{
    Data *d = Data::get_instance();
    int nb_jours = d->get_nb_jours();
    int i;

    float temps_algo = temps_limite_secondes / nb_jours;

    for (i = 0; i < nb_jours; i++)
    {
        this->algos.push_back(Algo_Genetique(i, taille_population, taux_croisement, taux_mutation, temps_algo));
    }
}

Algo_Resolution::~Algo_Resolution() {}

void Algo_Resolution::run()
{
    Data *d = Data::get_instance();
    int nb_jours = d->get_nb_jours();
    int i;

    for (i = 0; i < nb_jours; i++)
    {
        this->solution.push_back(this->algos[i].optimiser());
    }
    this->evaluer();
    this->afficher();
    this->output_txt();
}

void Algo_Resolution::evaluer()
{
    Data *d = Data::get_instance();
    int i;
    int nb_interfaces = d->get_nb_interfaces();
    float distance_totale = 0, ecart_type = 0;
    vector<float> distances_interface(nb_interfaces, 0.0);
    for (std::vector<Individu>::iterator it = begin(solution); it != end(solution); ++it)
    {
        std::transform(distances_interface.begin(), distances_interface.end(), it->distance_parcourues().begin(), distances_interface.begin(), std::plus<float>());
    }
    // Distance totale + Moyenne
    for (i = 0; i < nb_interfaces; i++)
    {
        distance_totale += distances_interface[i];
    }
    this->moyenne = distance_totale / nb_interfaces;

    // Ecart-type
    for (int i = 0; i < nb_interfaces; i++)
    {
        ecart_type += pow((distances_interface[i] - this->moyenne), 2);
    }
    this->ecart_type = sqrt(ecart_type / nb_interfaces);

    // Facteur de correlation
    this->facteur_corr = distance_totale / d->get_nb_formations();

    // Penalite
    this->calc_penalites();

    this->fitness = 0.5 * (this->moyenne + this->ecart_type) + 0.5 * this->facteur_corr * this->penalites;
}

void Algo_Resolution::calc_penalites()
{
    Data *d = Data::get_instance();
    int i, j, k;
    this->penalites = 0;
    for (i = 0; i < d->get_nb_jours(); i++)
    {
        for (int j = 0; j < d->get_taille_formations()[i]; j++)
        {
            int specialite_centre = this->solution[i].centres[j].get_specialite();
            if (this->solution[i].interfaces[j].get_specialite()[specialite_centre] != 1)
            {
                this->penalites++;
            }
        }
    }
    for (i = 0; i < d->get_nb_interfaces(); i++)
    {
        int heures_semaine = 0;
        for (j = 0; j < d->get_nb_jours(); j++)
        {
            for (k = 0; k < d->get_taille_formations()[j]; k++)
            {
                if (this->solution[j].interfaces[k].get_id() == i)
                {
                    heures_semaine += solution[j].formations[k].get_heure_fin() - solution[j].formations[k].get_heure_debut();
                }
            }
        }
        int heures_sup = heures_semaine - 35;
        if (heures_sup > 0)
        {
            this->penalites += heures_sup;
        }
    }
}

void Algo_Resolution::afficher()
{
    Data *d = Data::get_instance();
    int nb_jours = d->get_nb_jours();
    int i, j, k;
    string jours[6] = {"LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"};

    cout << endl
         << "_________________________________Solution_________________________________" << endl
         << endl;
    float fitness_cumulee = 0, penalites_cumulees = 0, ecart_cumule = 0, moyenne_cumulee = 0, moyenne_facteur = 0;
    for (i = 0; i < nb_jours; i++)
    {
        fitness_cumulee += this->solution[i].fitness;
        penalites_cumulees += this->solution[i].penalites;
        ecart_cumule += this->solution[i].ecart_type;
        moyenne_cumulee += this->solution[i].moyenne;
        moyenne_facteur += this->solution[i].facteur_corr;
    }
    cout << "Fitness de la semaine                                       : " << this->fitness << endl;
    cout << "Fitness cumulée des jours                                   : " << fitness_cumulee << endl;
    cout << "Penalites de la semaine                                     : " << this->penalites << endl;
    cout << "Pénalités cumulées des jours                                : " << penalites_cumulees << endl;
    cout << "Moyenne des distances par interface à la semaine            : " << this->moyenne << endl;
    cout << "Moyenne des moyennes des distances par interface par jour   : " << moyenne_cumulee / nb_jours << endl;
    cout << "Ecart-type des distances par interface à la semaine         : " << this->ecart_type << endl;
    cout << "Moyenne des ecarts-type des jour                            : " << ecart_cumule / nb_jours << endl;
    cout << "Facteur de correlation à la semaine                         : " << this->facteur_corr << endl;
    cout << "Moyenne des facteurs de correlation des jours               : " << moyenne_facteur / nb_jours << endl;

    for (i = 0; i < nb_jours; i++)
    {
        cout << endl
             << "---------------------------" << jours[i] << "---------------------------" << endl;

        cout << "   STATS" << endl;
        cout << "       fitness      : " << this->solution[i].fitness << endl
             << "       pénalités    : " << this->solution[i].penalites << endl
             << "       moyenne      : " << this->solution[i].moyenne << endl
             << "       écart-type   : " << this->solution[i].ecart_type << endl
             << "       facteur-corr : " << this->solution[i].facteur_corr << endl;

        for (j = 0; j < this->solution[i].taille; j++)
        {
            int nb_specialite_interface = 0;
            cout << "   " << this->solution[i].formations[j].get_heure_debut() << " h" << endl;

            cout << "   .   INTERFACE ";
            if (this->solution[i].interfaces[j].get_id() < 10)
                cout << " ";
            cout << this->solution[i].interfaces[j].get_id()
                 << " : specialite ";
            for (k = 0; k < d->get_nb_specialites(); k++)
            {
                if (this->solution[i].interfaces[j].get_specialite()[k] == 1)
                {
                    cout << k << " ";
                    nb_specialite_interface++;
                }
            }
            if (nb_specialite_interface == 0)
            {
                cout << "  ";
            }
            cout << "| competence ";
            for (k = 0; k < 2; k++)
            {
                if (this->solution[i].interfaces[j].get_competence()[k] == 1)
                {
                    cout << k << " ";
                }
            }

            cout << endl
                 << "   .   FORMATION ";
            if (this->solution[i].formations[j].get_id() < 10)
                cout << " ";
            cout << this->solution[i].formations[j].get_id()
                 << " : specialite " << this->solution[i].formations[j].get_specialite();
            if (nb_specialite_interface != 0)
            {
                for (k = 0; k < 2 * nb_specialite_interface - 1; k++)
                {
                    cout << " ";
                }
            }
            else
            {
                cout << " ";
            }
            cout << "| competence " << this->solution[i].formations[j].get_competence() << endl;

            cout << "   .   CENTRE    ";
            if (this->solution[i].centres[j].get_id() < 10)
                cout << " ";
            cout << this->solution[i].centres[j].get_id()
                 << " : specialite " << this->solution[i].centres[j].get_specialite();
            if (nb_specialite_interface != 0)
            {
                for (k = 0; k < 2 * nb_specialite_interface - 1; k++)
                {
                    cout << " ";
                }
            }
            else
            {
                cout << " ";
            }
            cout << "|" << endl;

            cout << "   " << this->solution[i].formations[j].get_heure_fin() << " h" << endl;
        }
    }
    cout << "______________________________________________________________" << endl;
}

void Algo_Resolution::output_txt()
{
    fstream file;
    file.open("./solution.txt", ios::out);
    if (!file)
    {
        cout << "Error creating solution.txt";
    }
    else
    {
        Data *d = Data::get_instance();
        int nb_jours = d->get_nb_jours();
        int i, j, k;
        string jours[6] = {"LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"};

        file << "_________________________________Solution_________________________________" << endl
             << endl;
        float fitness_cumulee = 0, penalites_cumulees = 0, ecart_cumule = 0, moyenne_cumulee = 0, moyenne_facteur = 0;
        for (i = 0; i < nb_jours; i++)
        {
            fitness_cumulee += this->solution[i].fitness;
            penalites_cumulees += this->solution[i].penalites;
            ecart_cumule += this->solution[i].ecart_type;
            moyenne_cumulee += this->solution[i].moyenne;
            moyenne_facteur += this->solution[i].facteur_corr;
        }
        file << "Fitness de la semaine                                       : " << this->fitness << endl;
        file << "Fitness cumulée des jours                                   : " << fitness_cumulee << endl;
        file << "Penalites de la semaine                                     : " << this->penalites << endl;
        file << "Pénalités cumulées des jours                                : " << penalites_cumulees << endl;
        file << "Moyenne des distances par interface à la semaine            : " << this->moyenne << endl;
        file << "Moyenne des moyennes des distances par interface par jour   : " << moyenne_cumulee / nb_jours << endl;
        file << "Ecart-type des distances par interface à la semaine         : " << this->ecart_type << endl;
        file << "Moyenne des ecarts-type des jour                            : " << ecart_cumule / nb_jours << endl;
        file << "Facteur de correlation à la semaine                         : " << this->facteur_corr << endl;
        file << "Moyenne des facteurs de correlation des jours               : " << moyenne_facteur / nb_jours << endl;

        for (i = 0; i < nb_jours; i++)
        {
            file << endl
                 << "---------------------------" << jours[i] << "---------------------------" << endl;

            file << "   STATS" << endl;
            file << "       fitness      : " << this->solution[i].fitness << endl
                 << "       pénalités    : " << this->solution[i].penalites << endl
                 << "       moyenne      : " << this->solution[i].moyenne << endl
                 << "       écart-type   : " << this->solution[i].ecart_type << endl
                 << "       facteur-corr : " << this->solution[i].facteur_corr << endl;

            for (j = 0; j < this->solution[i].taille; j++)
            {
                int nb_specialite_interface = 0;
                file << "   " << this->solution[i].formations[j].get_heure_debut() << " h" << endl;

                file << "   .   INTERFACE ";
                if (this->solution[i].interfaces[j].get_id() < 10)
                    file << " ";
                file << this->solution[i].interfaces[j].get_id()
                     << " : specialite ";
                for (k = 0; k < d->get_nb_specialites(); k++)
                {
                    if (this->solution[i].interfaces[j].get_specialite()[k] == 1)
                    {
                        file << k << " ";
                        nb_specialite_interface++;
                    }
                }
                if (nb_specialite_interface == 0)
                {
                    file << "  ";
                }
                file << "| competence ";
                for (k = 0; k < 2; k++)
                {
                    if (this->solution[i].interfaces[j].get_competence()[k] == 1)
                    {
                        file << k << " ";
                    }
                }

                file << endl
                     << "   .   FORMATION ";
                if (this->solution[i].formations[j].get_id() < 10)
                    file << " ";
                file << this->solution[i].formations[j].get_id()
                     << " : specialite " << this->solution[i].formations[j].get_specialite();
                if (nb_specialite_interface != 0)
                {
                    for (k = 0; k < 2 * nb_specialite_interface - 1; k++)
                    {
                        file << " ";
                    }
                }
                else
                {
                    file << " ";
                }
                file << "| competence " << this->solution[i].formations[j].get_competence() << endl;

                file << "   .   CENTRE    ";
                if (this->solution[i].centres[j].get_id() < 10)
                    file << " ";
                file << this->solution[i].centres[j].get_id()
                     << " : specialite " << this->solution[i].centres[j].get_specialite();
                if (nb_specialite_interface != 0)
                {
                    for (k = 0; k < 2 * nb_specialite_interface - 1; k++)
                    {
                        file << " ";
                    }
                }
                else
                {
                    file << " ";
                }
                file << "|" << endl;

                file << "   " << this->solution[i].formations[j].get_heure_fin() << " h" << endl;
            }
        }
        file << "______________________________________________________________" << endl;
        file.close();
    }
}