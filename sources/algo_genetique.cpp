#include "../headers/algo_genetique.h"

#define POURCENTAGE_MEILLEUR_INDIV 0.2

using namespace std;
using namespace std::chrono;

Algo_Genetique::Algo_Genetique() {}

Algo_Genetique::Algo_Genetique(int jour, int taille_population, float taux_croisement, float taux_mutation, float temps_limite_secondes)
{
    this->jour = jour;
    this->taille_population = taille_population;
    this->taux_croisement = taux_croisement;
    this->taux_mutation = taux_mutation;
    this->temps_limite = temps_limite_secondes * 1000;
}

Algo_Genetique::~Algo_Genetique() {}

Individu Algo_Genetique::optimiser()
{
    cout << "-------------Optimisation jour " << this->jour << "-------------" << endl;
    int temps_exec;
    this->generation_pop_aleatoire();
    // this->generation_pop_pourcentage_meilleur();
    // this->generation_pop_meilleur();
    // this->generation_pop_uniforme();
    Individu meilleur_individu = this->population->meilleur_individu();

    auto start = high_resolution_clock::now();
    do
    {
        this->population->croisements(this->taux_croisement);
        this->population->mutations(this->taux_mutation);

        Individu tmp_meilleur = this->population->meilleur_individu();
        if (meilleur_individu.fitness > tmp_meilleur.fitness)
        {
            cout << "Nouveau meilleur individu de fitness " << tmp_meilleur.fitness << endl;
            meilleur_individu = tmp_meilleur;
        }

        auto duration = duration_cast<milliseconds>(high_resolution_clock::now() - start);
        temps_exec = duration.count();
    } while (temps_exec < this->temps_limite);

    return meilleur_individu;
}

/*
 * Méthodes de génération d'individus et de population  
 */

Individu Algo_Genetique::generation_indiv_uniforme()
{
    Data *d = Data::get_instance();
    int index_formation;
    int nb_formations = d->get_taille_formations()[this->jour];

    vector<Formation> formations = d->get_formations()[this->jour];
    this->ordonner_formations(formations, nb_formations);
    vector<Interface> interfaces;
    vector<Centre> centres;

    vector<int> compteurs_interfaces(d->get_nb_interfaces(), 0);
    vector<int> compteurs_centres(d->get_nb_centres(), 0);

    for (index_formation = 0; index_formation < nb_formations; index_formation++)
    {
        interfaces.push_back(Interface());
        centres.push_back(Centre());
    }

    for (index_formation = 0; index_formation < nb_formations; index_formation++)
    {
        int min = *min_element(compteurs_interfaces.begin(), compteurs_interfaces.end());
        int index_min = -1;
        do
        {
            index_min++;
            if (index_min == d->get_nb_interfaces())
            {
                min += 1;
                index_min = 0;
            }
            interfaces[index_formation] = d->get_interfaces()[index_min];
        } while (min != compteurs_interfaces[index_min] || !Tool::are_interfaces_valides(formations, interfaces, index_formation + 1));
        compteurs_interfaces[index_min]++;

        min = *min_element(compteurs_centres.begin(), compteurs_centres.end());
        index_min = -1;
        do
        {
            index_min++;
            if (index_min == d->get_nb_centres())
            {
                min += 1;
                index_min = 0;
            }
            centres[index_formation] = d->get_centres()[index_min];
        } while (min != compteurs_centres[index_min] || !Tool::are_centres_valides(formations, centres, index_formation + 1));
        compteurs_centres[index_min]++;
    }

    return Individu(nb_formations, formations, interfaces, centres);
}

Individu Algo_Genetique::generation_indiv_aleatoire()
{

    Data *d = Data::get_instance();
    int index_formation;
    int nb_formations = d->get_taille_formations()[this->jour];

    vector<Formation> formations = d->get_formations()[this->jour];
    this->ordonner_formations(formations, nb_formations);
    vector<Interface> interfaces;
    vector<Centre> centres;

    for (index_formation = 0; index_formation < nb_formations; index_formation++)
    {
        interfaces.push_back(Interface());
        centres.push_back(Centre());
    }

    int max_tentative = pow(d->get_nb_interfaces(), 2);
    index_formation = 0;
    while (index_formation < nb_formations)
    {
        int compteur_tentative = 0;
        do
        {
            int index_interface = Tool::random_int(d->get_nb_interfaces());
            int index_centre = 0;
            while (d->get_centres()[index_centre].get_specialite() != formations[index_formation].get_specialite())
            {
                index_centre++;
            }
            interfaces[index_formation] = d->get_interfaces()[index_interface];
            centres[index_formation] = d->get_centres()[index_centre];
            compteur_tentative++;

        } while (!Tool::is_indiv_valide(formations, interfaces, centres, index_formation + 1) && compteur_tentative < max_tentative);
        // Regénéaration depuis le début si blocage
        if (compteur_tentative >= max_tentative)
        {
            index_formation = -1;
        }
        index_formation++;
    }

    return Individu(nb_formations, formations, interfaces, centres);
}

void Algo_Genetique::generation_pop_uniforme()
{
    int i;

    vector<Individu> population;

    for (i = 0; i < this->taille_population; i++)
    {
        population.push_back(Algo_Genetique::generation_indiv_uniforme());
    }
    this->population = new Population(this->taille_population, population);
}

void Algo_Genetique::generation_pop_aleatoire()
{
    int i;

    vector<Individu> population;

    for (i = 0; i < this->taille_population; i++)
    {
        population.push_back(Algo_Genetique::generation_indiv_aleatoire());
    }

    this->population = new Population(this->taille_population, population);
}

void Algo_Genetique::generation_pop_pourcentage_meilleur()
{
    vector<Individu> meilleur_population, nouvelle_population, meilleurs_individus;
    int i, j, index_meilleur_indiv, nb_meilleurs, taille_block;

    nb_meilleurs = POURCENTAGE_MEILLEUR_INDIV * this->taille_population;
    taille_block = 1 / POURCENTAGE_MEILLEUR_INDIV;

    for (i = 0; i < this->taille_population; i++)
    {
        nouvelle_population.push_back(Algo_Genetique::generation_indiv_aleatoire());
    }
    for (i = 0; i < nb_meilleurs; i++)
    {
        index_meilleur_indiv = 0;
        for (j = 1; j < this->taille_population - i; j++)
        {
            if (nouvelle_population[j].fitness < nouvelle_population[index_meilleur_indiv].fitness)
            {
                index_meilleur_indiv = j;
            }
        }
        meilleurs_individus.push_back(Individu(nouvelle_population[index_meilleur_indiv]));
        nouvelle_population.erase(nouvelle_population.begin() + index_meilleur_indiv);
    }
    for (i = 0; i < nb_meilleurs; i++)
    {
        for (j = 0; j < taille_block; j++)
        {
            meilleur_population.push_back(meilleurs_individus[i]);
        }
    }

    this->population = new Population(this->taille_population, meilleur_population);
}

void Algo_Genetique::generation_pop_meilleur()
{
    int i;
    vector<Individu> population;
    Individu nouvel_individu;
    Individu meilleur_individu = Algo_Genetique::generation_indiv_aleatoire();

    for (i = 1; i < this->taille_population; i++)
    {
        nouvel_individu = Algo_Genetique::generation_indiv_aleatoire();
        if (nouvel_individu.fitness < meilleur_individu.fitness)
        {
            meilleur_individu = Individu(nouvel_individu);
        }
    }

    for (i = 0; i < this->taille_population; i++)
    {
        population.push_back(Individu(meilleur_individu));
    }

    this->population = new Population(this->taille_population, population);
}

void Algo_Genetique::afficher()
{
    this->population->afficher();
}

void Algo_Genetique::ordonner_formations(vector<Formation> formations, int taille)
{
    int sorted = 0, i = taille - 1;
    while (i > 0 and !sorted)
    {
        sorted = 0;
        int j = 0;
        while (j < i)
        {
            sorted = 1;
            if (formations[j + 1].get_heure_debut() < formations[j].get_heure_debut())
            {
                iter_swap(formations.begin() + (j + 1), formations.begin() + j);
                sorted = 0;
            }
            j++;
        }
        i--;
    }
}