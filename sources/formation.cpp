#include "../headers/formation.h"

using namespace std;

Formation::Formation() {}

Formation::Formation(int id, int specialite, int competence, int heure_debut, int heure_fin)
{
    this->id = id;
    this->specialite = specialite;
    this->competence = competence;
    this->heure_debut = heure_debut;
    this->heure_fin = heure_fin;
}

Formation::~Formation() {}

int Formation::get_specialite()
{
    return this->specialite;
}
int Formation::get_competence()
{
    return this->competence;
}
int Formation::get_heure_debut()
{
    return this->heure_debut;
}
int Formation::get_heure_fin()
{
    return this->heure_fin;
}
int Formation::get_id()
{
    return this->id;
}

void Formation::afficher()
{
    cout << "    formation " << this->id << endl;
    cout << "        specialité : " << this->specialite << endl;
    cout << "        compétence : " << this->competence << endl;
    cout << "        heure de début : " << this->heure_debut << endl;
    cout << "        heure de fin : " << this->heure_fin << endl;
}