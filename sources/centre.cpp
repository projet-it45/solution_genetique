#include "../headers/centre.h"

using namespace std;

Centre::Centre() {}

Centre::Centre(int id, int specialite, int *position)
{
    int i;
    this->id = id;
    this->specialite = specialite;
    for (i = 0; i < 2; i++)
        this->position[i] = position[i];
}

Centre::~Centre() {}

int Centre::get_specialite()
{
    return this->specialite;
}
int *Centre::get_position()
{
    return this->position;
}
int Centre::get_id()
{
    return this->id;
}

void Centre::afficher()
{
    cout << "     centre " << this->id << endl;
    cout << "        specialité : " << this->specialite << endl;
    cout << "        coordonnées : {" << this->position[0] << ", " << this->position[1] << "}" << endl;
}