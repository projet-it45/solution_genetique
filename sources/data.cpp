#include "../Generator/instance-80formations.h"

#include "../headers/data.h"

using namespace std;

Data *Data::Data_ = nullptr;

Data *Data::get_instance()
{
    if (Data_ == nullptr)
    {
        Data_ = new Data();
        Data_->initialiser();
    }
    return Data_;
}

Data::Data() {}

Data::~Data()
{
    delete this->Data_;
    delete[] this->interfaces;
    delete[] this->centres;
    delete[] this->formations;
    delete[] this->taille_formations;
}

Interface *Data::get_interfaces()
{
    return this->interfaces;
}
Centre *Data::get_centres()
{
    return this->centres;
}
vector<Formation> *Data::get_formations()
{
    return this->formations;
}
int *Data::get_taille_formations()
{
    return this->taille_formations;
}
int Data::get_nb_interfaces()
{
    return this->nb_interfaces;
}
int Data::get_nb_formations()
{
    return this->nb_formations;
}
int Data::get_nb_centres()
{
    return this->nb_centres;
}
int Data::get_nb_specialites()
{
    return this->nb_specialites;
}
int Data::get_nb_jours()
{
    return this->nb_jours;
}
int *Data::get_SESSAD()
{
    return this->SESSAD;
}

void Data::creer_formations()
{
    int i;
    this->formations = new vector<Formation>[NB_JOURS];
    this->taille_formations = new int[NB_JOURS];
    for (i = 0; i < NBR_FORMATIONS; i++)
    {
        Formation tmp = Formation(i, formation[i][1], formation[i][2], formation[i][4], formation[i][5]);
        this->formations[formation[i][3] - 1].push_back(tmp);
        this->taille_formations[formation[i][3] - 1] += 1;
    }
}

void Data::creer_interfaces()
{
    int i;
    this->interfaces = new Interface[NBR_INTERFACES];
    for (i = 0; i < NBR_INTERFACES; i++)
    {
        this->interfaces[i] = Interface(i, specialite_interfaces[i], competences_interfaces[i], NBR_SPECIALITES);
    }
}

void Data::creer_centres()
{
    int i;
    this->centres = new Centre[NBR_CENTRES_FORMATION];
    for (i = 0; i < NBR_CENTRES_FORMATION; i++)
    {
        this->centres[i] = Centre(i, i, coord[i + 1]);
    }
}

void Data::initialiser()
{
    this->nb_specialites = NBR_SPECIALITES;
    this->nb_interfaces = NBR_INTERFACES;
    this->SESSAD[0] = coord[0][0];
    this->SESSAD[1] = coord[0][1];
    this->nb_formations = NBR_FORMATIONS;
    this->nb_centres = NBR_CENTRES_FORMATION;
    this->nb_jours = SAMEDI;
    this->creer_centres();
    this->creer_interfaces();
    this->creer_formations();
}

void Data::afficher()
{
    int i;
    cout << "================DATA================" << endl;
    cout << "Nombre de formations : " << this->nb_formations << endl;
    cout << "Nombre d'interfaces' : " << this->nb_interfaces << endl;
    cout << "Nombre de centres : " << this->nb_centres << endl;
    cout << "Coordonnées SESSAD : {" << this->SESSAD[0] << ", " << this->SESSAD[1] << "}" << endl;
    cout << "Centres : " << endl;
    for (i = 0; i < this->nb_centres; i++)
    {
        this->centres[i].afficher();
    }
    cout << "Interfaces : " << endl;
    for (i = 0; i < this->nb_interfaces; i++)
    {
        this->interfaces[i].afficher();
    }
    cout << "Nombre de formations par jour :" << endl;
    for (i = 0; i < NB_JOURS; i++)
    {
        cout << "    jour " << i + 1 << " : " << this->taille_formations[i] << endl;
    }
    for (i = 0; i < NB_JOURS; i++)
    {
        cout << "Formation jour " << i + 1 << " :" << endl;
        vector<Formation>::iterator it;
        for (it = this->formations[i].begin(); it != this->formations[i].end(); it++)
        {
            Formation tmp = *it;
            tmp.afficher();
        }
    }
    cout << "====================================" << endl;
}