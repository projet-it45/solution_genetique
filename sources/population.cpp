#include "../headers/population.h"
#include "../lib/tool.h"

using namespace std;

Population::Population() {}

Population::Population(int taille, vector<Individu> individus)
{
    this->individus = individus;
    this->taille = taille;
    evaluer();
}

Population::~Population()
{
    //delete[] this->individus;
}

void Population::ordonner()
{
    int sorted;
    for (int i = this->taille - 1; i > 0; i--)
    {
        sorted = 1;
        for (int j = 0; j < i; j++)
        {
            if (this->individus[j + 1].fitness < this->individus[j].fitness)
            {
                //SWAP FORMATION
                iter_swap(this->individus.begin() + (j + 1), this->individus.begin() + j);
                sorted = 0;
            }
        }
        if (sorted == 1)
        {
            return;
        }
    }
}

void Population::evaluer()
{
    int i;
    float sum = 0;
    for (i = 0; i < this->taille; i++)
    {
        sum += this->individus[i].fitness;
    }
    this->fitness = sum / this->taille;
}

Individu Population::meilleur_individu()
{
    int i;
    Individu meilleur_individu = this->individus[0];
    float meilleure_fitness = meilleur_individu.fitness;
    for (i = 1; i < this->taille; i++)
    {
        if (meilleure_fitness > this->individus[i].fitness)
        {
            meilleur_individu = this->individus[i];
            meilleure_fitness = meilleur_individu.fitness;
        }
    }
    return meilleur_individu;
}

void Population::mutations(float taux_mutation)
{

    int i;
    float alea;
    for (i = 0; i < this->taille; i++)
    {
        alea = Tool::random_float(1);
        if (alea < taux_mutation)
        {
            this->muter(i);
        }
    }
}

void Population::croisements(float taux_croisement)
{
    this->ordonner();

    Individu Parent1 = this->selection_roulette();
    Individu Parent2 = this->selection_roulette();

    float alea = Tool::random_float(1);

    if (alea < taux_croisement)
    {
        this->croiser(Parent1, Parent2, 5);
    }
    this->individus.push_back(Parent1);
    this->taille++;
    this->individus.push_back(Parent2);
    this->taille++;

    this->ordonner();
    this->evaluer();
}

Individu &Population::selection_roulette()
{
    float somme_fitness = 0;
    for (int i = 0; i < taille; i++)
    {
        somme_fitness += this->individus[i].fitness;
    }

    vector<float> cumulative_proba;
    cumulative_proba.reserve(taille);
    cumulative_proba[0] = (somme_fitness - this->individus[0].fitness) / somme_fitness;
    for (int i = 1; i < taille; i++)
    {
        cumulative_proba[i] = (somme_fitness - this->individus[i].fitness / somme_fitness) + cumulative_proba[i - 1];
    }

    float hungergames = Tool::random_float(1);

    int selected_indiv = 0;
    while (cumulative_proba[selected_indiv] < hungergames)
    {
        selected_indiv++;
    }

    Individu *indiv = new Individu(this->individus[selected_indiv]);
    this->individus.erase(individus.begin() + selected_indiv);
    this->taille--;
    return *indiv;
}

void Population::muter(int index_individu)
{
    vector<Interface> muted_interfaces = this->individus[index_individu].interfaces;
    Data *d = Data::get_instance();
    int random_int;
    int random_int2;
    do
    {
        muted_interfaces = this->individus[index_individu].interfaces;
        random_int = Tool::random_int(muted_interfaces.size() - 1);
        random_int2 = Tool::random_int(d->get_nb_interfaces() - 1);
        muted_interfaces[random_int] = d->get_interfaces()[random_int2];
    } while (!(Tool::are_interfaces_valides(this->individus[index_individu].formations, muted_interfaces, muted_interfaces.size())));

    this->individus[index_individu].interfaces = muted_interfaces;
    this->individus[index_individu].evaluer();
}

void Population::croiser(Individu &parent1, Individu &parent2, int point)
{

    vector<Centre> centres_p1;

    vector<Formation> formations_p1;
    vector<Interface> interfaces_p1;

    vector<Centre> centres_p2;
    vector<Formation> formations_p2;
    vector<Interface> interfaces_p2;
    //Croisement until 2 valids indivues
    int nb_genes = parent1.formations.size();
    do
    {
        //Reset Centres P1
        centres_p1 = parent1.centres;

        formations_p1 = parent1.formations;
        interfaces_p1 = parent1.interfaces;
        //Reset Centres P2
        centres_p2 = parent2.centres;
        formations_p2 = parent2.formations;
        interfaces_p2 = parent2.interfaces;
        //Swap

        for (int i = point; i < nb_genes; i++)
        {
            iter_swap(centres_p1.begin() + i, centres_p2.begin() + i);
            iter_swap(formations_p1.begin() + i, formations_p2.begin() + i);
            iter_swap(interfaces_p1.begin() + i, interfaces_p2.begin() + i);
        }
        point = Tool::random_int(nb_genes);
    } while (!(Tool::is_indiv_valide(formations_p1, interfaces_p1, centres_p1, nb_genes)) ||
             !(Tool::is_indiv_valide(formations_p2, interfaces_p2, centres_p2, nb_genes)));

    parent1.centres = centres_p1;
    parent1.interfaces = interfaces_p1;
    parent1.formations = formations_p1;

    parent2.centres = centres_p2;
    parent2.interfaces = interfaces_p2;
    parent2.formations = formations_p2;

    parent1.evaluer();
    parent2.evaluer();
}

void Population::afficher()
{
    int i;
    cout << "-----------Population-----------" << endl;
    cout << "Taille : " << this->taille << endl;
    cout << "Fitness : " << this->fitness << endl;
    for (i = 0; i < this->taille; i++)
    {
        this->individus[i].afficher();
    }
    cout << "--------------------------------" << endl;
}
