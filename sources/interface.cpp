#include "../headers/interface.h"

using namespace std;

Interface::Interface() {}

Interface::Interface(int id, int *specialite, int competence[2], int nb_specialite)
{
    int i;
    this->id = id;
    this->nb_specialite = nb_specialite;
    this->specialite = new int(nb_specialite);
    for (i = 0; i < nb_specialite; i++)
        this->specialite[i] = specialite[i];
    for (i = 0; i < 2; i++)
        this->competence[i] = competence[i];
}

Interface::~Interface()
{
    // delete[] this->specialite;
}

int *Interface::get_specialite()
{
    return this->specialite;
}
int *Interface::get_competence()
{
    return this->competence;
}
int Interface::get_id()
{
    return this->id;
}

void Interface::afficher()
{
    int i;
    cout << "    interface " << this->id << endl;
    cout << "        specialités : { ";
    for (i = 0; i < this->nb_specialite; i++)
    {
        cout << this->specialite[i] << " ";
    }
    cout << "}" << endl;
    cout << "        compétences : {" << this->competence[0] << ", " << this->competence[1] << "}" << endl;
}