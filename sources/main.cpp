#include <stdlib.h>
#include <unistd.h>
#include <cstdio>
#include <chrono>

#include "../headers/algo_resolution.h"
#include "../lib/tool.h"

using namespace std;

void param_error()
{
    cout << "4 paramètres requis" << endl;
    cout << "  - tailles des populations (superieure ou égale à 10) : entier" << endl;
    cout << "  - taux de croisement : réel entre 0 et 1" << endl;
    cout << "  - taux de mutation : réel entre 0 et 1" << endl;
    cout << "  - temps d'execution en seconde : réel" << endl;
}

int main(int argc, char *argv[])
{
    Tool::initialize_random();

    if (argc != 5)
    {
        param_error();
    }
    else
    {
        int taille_population = atoi(argv[1]);
        float taux_croisement = stof(argv[2]);
        float taux_mutation = stof(argv[3]);
        float temps_limite_secondes = stof(argv[4]);
        if (taille_population < 10 || taux_croisement > 1 || taux_croisement < 0 || taux_mutation > 1 || taux_mutation < 0 || temps_limite_secondes < 0)
        {
            cout << "Paramètres incorrectes" << endl;
            param_error();
        }
        else
        {
            Algo_Resolution algo = Algo_Resolution(taille_population, taux_croisement, taux_mutation, temps_limite_secondes);
            algo.run();
        }
    }
}