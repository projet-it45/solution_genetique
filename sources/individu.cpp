#include "../headers/individu.h"

using namespace std;

Individu::Individu() {}

Individu::Individu(int taille, vector<Formation> formations, vector<Interface> interfaces, vector<Centre> centres)
{
    this->taille = taille;
    this->formations = formations;
    this->interfaces = interfaces;
    this->centres = centres;
    this->evaluer();
}

Individu::Individu(Individu *indiv)
{
    this->taille = indiv->taille;
    this->formations = indiv->formations;
    this->interfaces = indiv->interfaces;
    this->centres = indiv->centres;
    this->moyenne = indiv->moyenne;
    this->ecart_type = indiv->ecart_type;
    this->facteur_corr = indiv->facteur_corr;
    this->penalites = indiv->penalites;
}

Individu::~Individu() {}

// Ordonne l'individu dans l'ordre chronologoqie de la journée.
void Individu::ordonner()
{
    int sorted;
    for (int i = this->formations.size() - 1; i > 0; i--)
    {
        sorted = 1;
        for (int j = 0; j < i; j++)
        {
            if (this->formations[j + 1].get_heure_debut() < this->formations[j].get_heure_debut())
            {
                //SWAP FORMATION
                iter_swap(this->formations.begin() + (j + 1), this->formations.begin() + j);
                //SWAP CENTRE
                iter_swap(this->centres.begin() + (j + 1), this->centres.begin() + j);
                //SWAP INTERFACE
                iter_swap(this->interfaces.begin() + (j + 1), this->interfaces.begin() + j);
                sorted = 0;
            }
        }
        if (sorted == 1)
        {
            return;
        }
    }
}

void Individu::evaluer()
{
    this->ordonner();
    vector<float> stats = statistiques();
    this->moyenne = stats[0];
    this->ecart_type = stats[1];
    this->facteur_corr = stats[2];

    this->calc_penalites();

    this->fitness = 0.5 * (stats[0] + stats[1] + 0.5 * stats[2] * this->penalites);
}

void Individu::afficher(bool affichage_par_interface)
{
    Data *d = Data::get_instance();

    int i, j;
    cout << "---------------Individu---------------" << endl;
    cout << "taille : " << this->taille << endl;
    cout << "fitness : " << this->fitness << endl;
    if (!affichage_par_interface)
    {
        cout << "Planning de la journée" << endl;
        for (i = 0; i < this->taille; i++)
        {
            cout << "Mission " << i << endl;
            this->formations[i].afficher();
            this->interfaces[i].afficher();
            this->centres[i].afficher();
        }
    }
    else
    {
        for (i = 0; i < d->get_nb_interfaces(); i++)
        {
            cout << "Interface " << i << endl;
            for (j = 0; j < this->taille; j++)
            {
                if (this->interfaces[j].get_id() == i)
                {
                    this->formations[j].afficher();
                    this->centres[j].afficher();
                }
            }
        }
    }
    cout << "--------------------------------------" << endl;
}

// Return a vector of float : 0 -> Moyenne , 1 -> Ecart type , 2-> Facteur de correlation
vector<float> Individu::statistiques()
{
    Data *d = Data::get_instance();
    int nb_interfaces = d->get_nb_interfaces();
    vector<float> interface_distance = distance_parcourues();
    float distance_totale = 0;
    for (int i = 0; i < nb_interfaces; i++)
    {
        distance_totale += interface_distance[i];
    }
    float moyenne = distance_totale / nb_interfaces;
    float correlation_factor = distance_totale / taille;

    float somme_valeur_abs = 0;
    for (int i = 0; i < nb_interfaces; i++)
    {
        somme_valeur_abs += pow((interface_distance[i] - moyenne), 2);
    }

    float standard_deviation = sqrt(somme_valeur_abs / nb_interfaces);

    vector<float> statistiques;
    statistiques.reserve(3);
    statistiques[0] = moyenne;
    statistiques[1] = standard_deviation;
    statistiques[2] = correlation_factor;
    return statistiques;
}

//Retourne un vecteur contenant les distances pour chaque interfaces
vector<float> Individu::distance_parcourues()
{
    Data *d = Data::get_instance();
    int nb_interfaces = d->get_nb_interfaces();
    int interface_centre_precedent[nb_interfaces][2];
    vector<float> interface_distance(nb_interfaces, 0);
    //SESSAD FOR EVERYONE
    for (int i = 0; i < nb_interfaces; i++)
    {
        interface_centre_precedent[i][0] = d->get_SESSAD()[0];
        interface_centre_precedent[i][1] = d->get_SESSAD()[1];
    }
    for (int i = 0; i < taille; i++)
    {
        //Interface présente
        int id_interface = interfaces[i].get_id();
        // Teste si pos-1 different de pos
        if (centres[i].get_position()[0] != interface_centre_precedent[id_interface][0] || centres[i].get_position()[1] != interface_centre_precedent[id_interface][1])
        {
            int x = centres[i].get_position()[0] - interface_centre_precedent[id_interface][0];
            int y = centres[i].get_position()[1] - interface_centre_precedent[id_interface][1];
            int pow1 = pow(x, 2);
            int pow2 = pow(y, 2);
            interface_distance[id_interface] += sqrt(pow1 + pow2);

            interface_centre_precedent[id_interface][0] = centres[i].get_position()[0];
            interface_centre_precedent[id_interface][1] = centres[i].get_position()[1];
        }
    }
    //RETOUR SESSAD
    for (int i = 0; i < nb_interfaces; i++)
    {
        int x = interface_centre_precedent[i][0];
        int y = interface_centre_precedent[i][1];
        if (x != d->get_SESSAD()[0] || y != d->get_SESSAD()[1])
        {
            x -= d->get_SESSAD()[0];
            y -= d->get_SESSAD()[1];
            int pow1 = pow(x, 2);
            int pow2 = pow(y, 2);
            interface_distance[i] += sqrt(pow1 + pow2);
        }
    }
    return interface_distance;
}
void Individu::calc_penalites()
{
    Data *d = Data::get_instance();
    int i, k;
    this->penalites = 0;

    for (int i = 0; i < this->taille; i++)
    {
        int specialite_centre = this->centres[i].get_specialite();
        if (this->interfaces[i].get_specialite()[specialite_centre] != 1)
        {
            this->penalites++;
        }
    }
    for (i = 0; i < d->get_nb_interfaces(); i++)
    {
        int heures_jour = 0;

        for (k = 0; k < this->taille; k++)
        {
            if (this->interfaces[k].get_id() == i)
            {
                heures_jour += this->formations[k].get_heure_fin() - this->formations[k].get_heure_debut();
            }
        }

        int heures_sup = heures_jour - 8;
        if (heures_sup > 0)
        {
            this->penalites += heures_sup;
        }
    }
}