#ifndef TOOL_H
#define TOOL_H

#include <stdlib.h>
#include <time.h>

class Tool
{
public:
    /*
    * Méthodes randomize  
    */
    static void initialize_random()
    {
        srand(time(NULL));
    };

    static int random_int(long max)
    {
        return (rand() % max);
    };

    static float random_float(float max)
    {
        return (float(rand()) / float(RAND_MAX) * max);
    };

    /*
    * Méthodes de vérification de la validité d'un individu  
    */
    static int is_indiv_valide(vector<Formation> formations, vector<Interface> interfaces, vector<Centre> centres, int taille)
    {
        {
            if (!(are_interfaces_valides(formations, interfaces, taille)) ||
                !(are_centres_valides(formations, centres, taille)))
            {
                return 0;
            }
            return 1;
        }
    }
    static int are_interfaces_valides(vector<Formation> formations, vector<Interface> interfaces, int taille)
    {
        Data *d = Data::get_instance();

        int nb_interfaces = d->get_nb_interfaces();
        int i, j;

        // Contraintes par interfaces
        for (i = 0; i < nb_interfaces; i++)
        {
            int nb_heures = 0;
            int precedente_heure_fin = 0;
            int min_heure_debut = 24;
            int max_heure_fin = 0;
            int heure_enchainees = 0;
            for (j = 0; j < taille; j++)
            {
                if (interfaces[j].get_id() == i)
                {
                    // Formations ne se superpose pas
                    if (j > 0 && precedente_heure_fin != 0)
                    {
                        if (precedente_heure_fin > formations[j].get_heure_debut())
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        precedente_heure_fin = formations[j].get_heure_fin();
                    }
                    // Moins de 7 heures par jours
                    nb_heures += formations[j].get_heure_fin() - formations[j].get_heure_debut();
                    // Journée de moins de 12 heures
                    if (min_heure_debut > formations[j].get_heure_debut())
                        min_heure_debut = formations[j].get_heure_debut();
                    if (max_heure_fin < formations[j].get_heure_fin())
                        max_heure_fin = formations[j].get_heure_fin();
                    // Moins de 6 heures d'affilées
                    if (heure_enchainees == 0 || formations[j].get_heure_debut() != precedente_heure_fin)
                    {
                        heure_enchainees = formations[j].get_heure_fin() - formations[j].get_heure_debut();
                    }
                    else
                    {
                        heure_enchainees += formations[j].get_heure_fin() - formations[j].get_heure_debut();
                    }
                    if (heure_enchainees > 6)
                    {
                        return 0;
                    }
                }
            }
            if (nb_heures > 7 || max_heure_fin - min_heure_debut > 12)
            {
                return 0;
            }
        }
        for (i = 0; i < taille; i++)
        {
            // Compétence de l'interface match la compétence de la formation OU Spécialité du centre match la spécialité de la formation
            if (interfaces[i].get_competence()[formations[i].get_competence()] != 1)
            {
                return 0;
            }
        }
        return 1;
    }

    static int are_centres_valides(vector<Formation> formations, vector<Centre> centres, int taille)
    {
        int i;

        // Contraintes par formation
        for (i = 0; i < taille; i++)
        {
            // Compétence de l'interface match la compétence de la formation OU Spécialité du centre match la spécialité de la formation
            if (centres[i].get_specialite() != formations[i].get_specialite())
            {
                return 0;
            }
        }
        return 1;
    }
};

#endif
