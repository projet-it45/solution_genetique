#ifndef ALGO_RESOLUTION_H
#define ALGO_RESOLUTION_H

#include "algo_genetique.h"

#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

class Algo_Resolution
{
private:
    vector<Algo_Genetique> algos;
    vector<Individu> solution;
    float fitness;
    float moyenne;
    float ecart_type;
    float facteur_corr;
    int penalites;

    void calc_penalites();
    void evaluer();
    void afficher();
    void output_txt();

public:
    Algo_Resolution(int taille_population, float taux_croisement, float taux_mutation, float temps_limite_secondes);
    ~Algo_Resolution();

    void run();
};

#endif