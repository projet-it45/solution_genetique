#ifndef POPULATION_H
#define POPULATION_H

#include "individu.h"

class Population
{
public:
    Population();
    Population(int taille, vector<Individu> individus);
    ~Population();

    int taille;
    vector<Individu> individus;
    float fitness;

    void mutations(float taux_mutation);
    void croisements(float taux_croisement);
    void afficher();
    Individu meilleur_individu();

private:
    void croiser(Individu &individu1, Individu &individu2, int point);
    void muter(int index_individu);
    void ordonner();
    void evaluer();
    Individu &selection_roulette();
};

#endif