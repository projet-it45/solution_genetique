#ifndef INDIVIDU_H
#define INDIVIDU_H

#include "algorithm"
#include "math.h"
#include <vector>
#include "data.h"
using namespace std;

class Individu
{
public:
    Individu();
    Individu(int taille, vector<Formation> formations, vector<Interface> interfaces, vector<Centre> centres);
    Individu(Individu *indiv);

    ~Individu();

    int taille;
    float fitness;
    float moyenne;
    float ecart_type;
    float facteur_corr;
    int penalites;

    vector<Formation> formations;
    vector<Interface> interfaces;
    vector<Centre> centres;

    void afficher(bool affichage_par_interface = false);
    void evaluer();
    vector<float> distance_parcourues();

private:
    vector<float> statistiques();
    void calc_penalites();
    void ordonner();
};

#endif