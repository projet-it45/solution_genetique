#ifndef INTERFACE_H
#define INTERFACE_H

#include <cstdlib>
#include <iostream>

class Interface
{
private:
    int id;
    int nb_specialite;
    int *specialite;
    int competence[2];

public:
    Interface();
    Interface(int id, int *specialite, int competence[2], int nb_specialite);
    ~Interface();

    int *get_specialite();
    int *get_competence();
    int get_id();

    void afficher();
};

#endif