#ifndef DATA_H
#define DATA_H

#include <iostream>
#include <stdio.h>
#include <vector>
#include "formation.h"
#include "centre.h"
#include "interface.h"

using namespace std;

#define NB_JOURS 6

class Centre;
class Interface;
class Formation;

class Data
{
protected:
    Data();
    ~Data();

    static Data *Data_;

    Interface *interfaces;
    Centre *centres;
    vector<Formation> *formations;
    int *taille_formations;
    int nb_interfaces;
    int nb_formations;
    int nb_centres;
    int nb_specialites;
    int nb_jours;
    int SESSAD[2];

    void creer_formations();
    void creer_interfaces();
    void creer_centres();
    void initialiser();

public:
    Data(Data &other) = delete;
    void operator=(const Data &) = delete;

    static Data *get_instance();

    Interface *get_interfaces();
    Centre *get_centres();
    vector<Formation> *get_formations();
    int *get_taille_formations();
    int get_nb_interfaces();
    int get_nb_formations();
    int get_nb_centres();
    int get_nb_specialites();
    int *get_SESSAD();
    int get_nb_jours();

    void afficher();
};

#endif