#ifndef FORMATION_H
#define FORMATION_H

#include <iostream>

class Formation
{
private:
    int id;
    int specialite;
    int competence;
    int heure_debut;
    int heure_fin;

public:
    Formation();
    Formation(int id, int specialite, int competence, int heure_debut, int heure_fin);
    ~Formation();

    int get_specialite();
    int get_competence();
    int get_heure_debut();
    int get_heure_fin();
    int get_id();

    void afficher();
};

#endif