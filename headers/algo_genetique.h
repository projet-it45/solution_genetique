#ifndef ALGO_GENETIQUE_H
#define ALGO_GENETIQUE_H

#include "population.h"
#include "../lib/tool.h"

#include <chrono>
#include <math.h>

class Algo_Genetique
{
private:
    int jour;
    int taille_population;
    float temps_limite;
    float taux_croisement;
    float taux_mutation;
    Population *population;

    Individu generation_indiv_uniforme();
    Individu generation_indiv_aleatoire();
    void generation_pop_uniforme();
    void generation_pop_aleatoire();
    void generation_pop_pourcentage_meilleur();
    void generation_pop_meilleur();

    void ordonner_formations(vector<Formation> formations, int taille);

public:
    Algo_Genetique();
    Algo_Genetique(int jour, int taille_population, float taux_croisement, float taux_mutation, float temps_limite_secondes);
    ~Algo_Genetique();

    Individu optimiser();
    void afficher();
};

#endif