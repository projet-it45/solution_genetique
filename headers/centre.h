#ifndef CENTRE_H
#define CENTRE_H

#include <cstdlib>
#include <iostream>

class Centre
{
private:
    int id;
    int specialite;
    int position[2];

public:
    Centre();
    Centre(int id, int specialite, int position[2]);
    ~Centre();

    int get_specialite();
    int *get_position();
    int get_id();

    void afficher();
};

#endif