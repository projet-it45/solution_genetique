MKDIR_P = mkdir -p
BUILD_DIR = ./build/
BIN_DIR = $(BUILD_DIR)bin/
OBJ_DIR = $(BUILD_DIR)obj/
SOURCES = ./sources/

LIBS= -lm

LIBS_PATH= \
	-L/lib \
	-L/usr/lib \
	-L/usr/local/lib

CC=g++

FLAGS=  -c -g -Wall -std=c++11

OBJ= \
	$(OBJ_DIR)main.o \
	$(OBJ_DIR)centre.o \
	$(OBJ_DIR)formation.o \
	$(OBJ_DIR)interface.o \
	$(OBJ_DIR)individu.o \
	$(OBJ_DIR)data.o \
	$(OBJ_DIR)population.o \
	$(OBJ_DIR)algo_genetique.o \
	$(OBJ_DIR)algo_resolution.o


optimiser : $(OBJ) 
	$(CC) -o $(BIN_DIR)optimiser $(OBJ) $(LIBS_PATH) $(LIBS)	

clean : 
	rm -rf $(OBJ_DIR)

clear : 
	rm -rf $(BUILD_DIR)

directories :
	$(MKDIR_P) $(BIN_DIR)
	$(MKDIR_P) $(OBJ_DIR)

$(OBJ_DIR)%.o : $(SOURCES)%.cpp
	make directories
	$(CC) $(FLAGS) -o $@ $<