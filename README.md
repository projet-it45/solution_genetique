# Projet IT45

Recherche de solutions au problème posé par implémentation d'algorthmes génétiques. 
On cherche à planifier une semaine optimale de travail sur 6 jours ouvrés.
Pour optimiser la semaine on optimise alors chaque jour grâce à un algorithme 
génétique puis on reconstitue la semaine dans un algorithme de 
résolution global.

## Prerequis

Compilateur c++.

## Lancement

Utiliser le makefile présent à la racine du projet pour générer l'executable :

    $ make

4 paramètres sont requis pour lancer l'optimisation :

  - *taille des populations*        : entier supérieur ou égale à 10
  - *taux de croisement*            : réel entre 0 et 1 
  - *taux de mutation*              : réel entre 0 et 1
  - *temps d'execution en secondes* : réel

Après exécution du makefile un dossier build est créé dans lequel on trouve deux sous dossiers.
Le dossier *obj* contient les fichiers objets *.o* et *bin* l'executable binaire *optimiser*.

Exemple d'exectution depuis la racine du projet :

    $ ./build/bin/optimiser 100 0.7 0.2 10

La commande *clean* permet de supprimer les objets et *clear* de supprimer le build :

    $ make clean
    $ make clear

## Instance du problème

Pour générer une instance utiliser le projet java nous ayant été donné avec le sujet.
Celui-ci est dsponible dans le projet dans le dossier *./Generator*.
Spécifier le fichier d'instance à utiliser directement dans le code de la classe *Data* :
  - *./source/data.cpp* ligne 1 : *"#include <chemin_instance .h>"*

## Méthode de génération des population initiale

Plusieurs méthodes de génération de populations initiales sont impléméntées (cf rapport).
Pour changer de méthode rendez-vous directement dans la classe *./sources/algo_genetique.cpp*.
Les 4 générations sont appelées aux lignes 25 à 28. Décommenter la génération à utiliser. 

## Auteur

  - **Owein DOURNEAU**
  - **Clément HASLE** 

*UTBM printemps 2021*